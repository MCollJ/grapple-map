import { Button } from "@mui/material";
import React from "react";

import "./UserButton.scss";

const UserButton = ({ imgSrc, onClick }) => (
  <Button onClick={onClick}>
    <img src={imgSrc} alt="profile" />
  </Button>
);

export default UserButton;
