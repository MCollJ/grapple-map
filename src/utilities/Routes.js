import React from "react";
import { Routes, Route } from "react-router-dom";

import Home from "../containers/pages/Home";

const AllRoutes = (props) => {
  return (
    <Routes>
      <Route path="/" element={<Home />}>
        <Route index element={<Home />} />
        {/* <Route exact component={LoginPage} path="/login"/> */}
        <Route component={CatchAllPage} />
      </Route>
    </Routes>
  );
};

//A protected route that redirects to login if user is not authenticated
// const PrivateRoute = ({ component: Component, ...rest }) => (
//   <Route {...rest} render={(props) => (
//     Api.hasToken() === true
//       ? <Component {...props} />
//       : <Redirect to={{
//           pathname: '/login',
//           state: { from: props.location }
//         }} />
//   )} />
// );

const CatchAllPage = () => (
  <div>Nope</div>
  // <ErrorPage heading="404 Error - page not found">
  // </ErrorPage>
);

export default AllRoutes;
